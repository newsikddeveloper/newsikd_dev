/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.comp;

import app.sikd.entity.mgr.TMenu;
import java.io.Serializable;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author sora
 */
public class MyTreeNode extends DefaultTreeNode implements Serializable{
    TMenu menu;
//    String iconnya="";

    public MyTreeNode(TMenu data, TreeNode parent) {
        super(data.getNama(), parent);
        menu = data;
//        this.iconnya = type;
//        super.setType(type);
    }
    public MyTreeNode(String nama, TreeNode parent) {
        super(nama, parent);
    }

    public TMenu getMenu() {
        return menu;
    }

    public void setMenu(TMenu menu) {
        this.menu = menu;
    }

//    public String getIconnya() {
//        return iconnya;
//    }
//
//    public void setIconnya(String iconnya) {
//        this.iconnya = iconnya;
//    }
//    
    
    
}
