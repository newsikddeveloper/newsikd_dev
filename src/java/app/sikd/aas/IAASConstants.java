package app.sikd.aas;

/**
 *
 * @author Sora
 */
public class IAASConstants {
    public static final String SESSION_USERNAME_KEY = "app.sikd.aas.UsernameCookieKey";
    public static final String SESSION_ID_KEY = "app.sikd.aas.SessionIDCookieKey";
    public static final String SESSION_GROUPTYPE_KEY = "app.sikd.aas.GroupTypeCookieKey";
    public static final String SESSION_GROUPID_KEY = "app.sikd.aas.GroupidCookieKey";
    
}
