package app.sikd.aas;

import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.FacesException;
import javax.faces.application.ProjectStage;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author sora
 */
public class DefaultExceptionHandler extends ExceptionHandlerWrapper {

    private final ExceptionHandler wrapped;

    public DefaultExceptionHandler(ExceptionHandler wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public ExceptionHandler getWrapped() {
        return this.wrapped;
    }

    @Override
    public void handle() throws FacesException {

        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext excontext = fc.getExternalContext();

        if (fc.isProjectStage(ProjectStage.Development)) {
            getWrapped().handle();
        } else {
            for (Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator(); i.hasNext();) {
                ExceptionQueuedEvent event = i.next();
                ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();

                String redirectPage = null;
                Throwable t = context.getException();

                try {
                  if (t instanceof ViewExpiredException) {
                        HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
                        if (session != null) {
                            session.invalidate();
                            SessionUtil.deleteCookies();
                            SessionUtil.deleteServletSession();
                            try {
                                excontext.redirect(ServerUtil.getDomain() + "/NewSIKD/nosession.jsf");
                            } catch (IOException ex) {
                                Logger.getLogger(DefaultExceptionHandler.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (Exception ex) {
                                Logger.getLogger(DefaultExceptionHandler.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                } finally {
                    i.remove();
                }
                break;
            }
        }
    }
}
