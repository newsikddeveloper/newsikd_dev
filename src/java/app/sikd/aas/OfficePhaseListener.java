package app.sikd.aas;

import app.sikd.entity.mgr.TUserAccount;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.web.Menu2MBean;
import app.sikd.web.UserMBean;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Sora
 */
public class OfficePhaseListener implements PhaseListener {

    public void afterPhase(PhaseEvent event) {
        FacesContext fc = event.getFacesContext();
        ExternalContext excontext = fc.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) excontext.getRequest();
        String URI = request.getRequestURI();
        if (URI.endsWith("index.jsf")) {
            System.out.println("masuk ke index.jsf");
            String sessionid = request.getParameter("sessionid");
            String username = request.getParameter("username");
            try {
                SessionUtil.deleteCookies();
                SessionUtil.deleteServletSession();
            } catch (Exception ex) {
                System.out.println("Masuk Exception");
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Kesalahan Proses Verifikasi User Antar Server", ex.toString());
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        } else {
            String username = SessionUtil.getCookieUsername();
            String idgs = SessionUtil.getCookieGroupid();
            if (username != null && idgs != null) {
                
            }else{
            if (!URI.endsWith("nosesi.jsf")) {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Pengguna tidak dikenal", "Maaf anda sebagai pengguna tidak dikenal atau masa sesi koneksi anda sudah habis. Silahkan lakukan proses login kembali.");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    try {
                        excontext.redirect(ServerUtil.getDomain() + FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/nosesi.jsf");
                    } catch (IOException ex) {
                        Logger.getLogger(OfficePhaseListener.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            /*System.out.println("masuk "+  URI);
            String username = SessionUtil.getCookieUsername();
            String idgs = SessionUtil.getCookieGroupid();
            if (username != null && idgs != null) {
                System.out.println("user " + username  + " grup  " + idgs);
                try {
                    long idGrup = Long.valueOf(idgs);
                    StringTokenizer st = new StringTokenizer(URI.trim(), "/");
                    String namamenu = "";
                    int iii = 0;
                    while (st.hasMoreTokens()) {
                        String ss = st.nextToken();
                        if (iii > 0) {
                            if (!namamenu.trim().equals("")) {
                                namamenu = namamenu + "/";
                            }
                            namamenu = namamenu + ss;
                        }
                        iii++;
                    }

                    Context ctxLogin = ServerUtil.getLoginContext();

                    NewLoginSessionBeanRemote loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
                    boolean bb = loginSession.canRead(idGrup, namamenu);                    
                    if (bb == false) {
                        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Pengguna tidak dikenal", "Maaf anda sebagai pengguna tidak dikenal atau masa sesi koneksi anda sudah habis. Silahkan lakukan proses login kembali.");
                        FacesContext.getCurrentInstance().addMessage(null, msg);
                        try {
                            excontext.redirect(ServerUtil.getDomain() + "/NewSIKD/nosesi.jsf");
                        } catch (IOException ex) {
                            Logger.getLogger(OfficePhaseListener.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    else {                        
                        FacesContext facesContext = event.getFacesContext();
                        System.out.println("ambil UserMBean");
                        ValueExpression valExp = facesContext.getApplication().getExpressionFactory().createValueExpression(facesContext.getELContext(), "#{userMBean}", UserMBean.class);
//                        System.out.println("setting MenuMBean");
//                        ValueExpression valExp2 = facesContext.getApplication().getExpressionFactory().createValueExpression(facesContext.getELContext(), "#{menuMBean}", MenuMBean.class);
//                        System.out.println("setting MenuMBean");
//                        MenuMBean menubean = (MenuMBean) valExp2.getValue(facesContext.getELContext());
                        System.out.println("setting UserMBean");
                        UserMBean userbean = (UserMBean) valExp.getValue(facesContext.getELContext());
                        System.out.println("db: getUserByName ");
                        TUserAccount tua = loginSession.getUserbyName(username);
                        if (userbean.getUserAccount() == null) {
                            System.out.println("Useraccount ada ");
                            System.out.println("init UserMBean ");
                            userbean.initData(tua);
//                            System.out.println("init User GRoup di MenuMBean ");
//                            menubean.setUserGroup(tua.getGroup());
//                            System.out.println("buat Menu di UserMBean");
//                            menubean.createMenu(tua.getMenus());
//                            System.out.println("buatMenu di UserMBean Sukses......................................");
                        }
                        
                        if (URI.trim().endsWith("ChartOfAccount.jsf")) {
                            
                            ValueExpression valExp3 = facesContext.getApplication().getExpressionFactory().createValueExpression(facesContext.getELContext(), "#{menu2MBean}", Menu2MBean.class);
                            Menu2MBean menu2bean = (Menu2MBean) valExp3.getValue(facesContext.getELContext());
                            menu2bean.setUserGroup(tua.getGroup());
                            menu2bean.createMenu(tua.getMenus());
                            
                        }
                    }
                } catch (Exception ex) {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Kesalahan: " + ex.toString(), ex.toString());
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
            } else {
                if (!URI.endsWith("nosesi.jsf")) {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Pengguna tidak dikenal", "Maaf anda sebagai pengguna tidak dikenal atau masa sesi koneksi anda sudah habis. Silahkan lakukan proses login kembali.");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    try {
                        excontext.redirect(ServerUtil.getDomain() + FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/nosesi.jsf");
                    } catch (IOException ex) {
                        Logger.getLogger(OfficePhaseListener.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            */
        }
    }

    public void beforePhase(PhaseEvent event) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

    void goToNoSessi(ExternalContext excontext, String smsg) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", smsg);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        try {
            System.out.println(ServerUtil.getDomain() + "/NewSIKD/nosesi.jsf");
            excontext.redirect(ServerUtil.getDomain() + "/NewSIKD/nosesi.jsf");
        } catch (IOException ex) {
            Logger.getLogger(OfficePhaseListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
