package app.sikd.aas;

import app.sikd.entity.mgr.TGroupRight;
import app.sikd.entity.mgr.TUserAccount;
import java.util.List;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author donfebi
 */
public class SessionUtil {
    static public void setUsernameCookie(String username){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext excontext = context.getExternalContext();
	HttpServletResponse httpServletResponse = (HttpServletResponse)excontext.getResponse();
        
        Cookie cookie = new Cookie(IAASConstants.SESSION_USERNAME_KEY, username);  
        cookie.setPath("/");
        httpServletResponse.addCookie(cookie);
    }
    
    static public void setUserGroupCookie(String idGroup){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext excontext = context.getExternalContext();
	HttpServletResponse httpServletResponse = (HttpServletResponse)excontext.getResponse();
        
        Cookie cookie = new Cookie(IAASConstants.SESSION_GROUPID_KEY, idGroup);  
        cookie.setPath("/");
        httpServletResponse.addCookie(cookie);
    }
    
//    static public void setTUserAccount(TUserAccount tua){
//        FacesContext context = FacesContext.getCurrentInstance();
//        ExternalContext excontext = context.getExternalContext();
//        HttpServletRequest req = (HttpServletRequest) excontext.getRequest();
//        req.getSession().setAttribute("tua", tua);
//    }
//    static public TUserAccount getTUserAccount(){
//        FacesContext context = FacesContext.getCurrentInstance();
//        ExternalContext excontext = context.getExternalContext();
//        HttpServletRequest req = (HttpServletRequest) excontext.getRequest();
//        return (TUserAccount)req.getSession().getAttribute("tua");
//    }
//    
//    static public void setGroupRight(List<TGroupRight> rights){
//        FacesContext context = FacesContext.getCurrentInstance();
//        ExternalContext excontext = context.getExternalContext();
//        HttpServletRequest req = (HttpServletRequest) excontext.getRequest();
//        req.getSession().setAttribute("rights", rights);
//    }
//    static public List<TGroupRight> getGroupRight(){
//        FacesContext context = FacesContext.getCurrentInstance();
//        ExternalContext excontext = context.getExternalContext();
//        HttpServletRequest req = (HttpServletRequest) excontext.getRequest();
//        return (List<TGroupRight>)req.getSession().getAttribute("rights");
//    }
    
    
    static public void setSessionIDCookie(String id){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext excontext = context.getExternalContext();
	HttpServletResponse httpServletResponse = (HttpServletResponse)excontext.getResponse();
        
        Cookie cookie = new Cookie(IAASConstants.SESSION_ID_KEY, id);  
        cookie.setPath("/");
        httpServletResponse.addCookie(cookie);
    }
    
    static public void setGroupTypeCookie(String grouptype){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext excontext = context.getExternalContext();
	HttpServletResponse httpServletResponse = (HttpServletResponse)excontext.getResponse();
        
        Cookie cookie = new Cookie(IAASConstants.SESSION_GROUPTYPE_KEY, grouptype);  
        cookie.setPath("/");
        httpServletResponse.addCookie(cookie);
    }

    static public String getCookieUsername(){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext excontext = context.getExternalContext();
        HttpServletRequest httpServletRequest = (HttpServletRequest)excontext.getRequest();

        Cookie[] cookies = httpServletRequest.getCookies();

        if (cookies != null) {
            for(int i=0; i<cookies.length; i++){
                String cname = cookies[i].getName();
                if (cname.equals(IAASConstants.SESSION_USERNAME_KEY)){ 
                    return cookies[i].getValue();
                }
            }
        }

        return null;
    }
    
    static public String getCookieGroupid(){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext excontext = context.getExternalContext();
        HttpServletRequest httpServletRequest = (HttpServletRequest)excontext.getRequest();

        Cookie[] cookies = httpServletRequest.getCookies();

        if (cookies != null) {
            for(int i=0; i<cookies.length; i++){
                String cname = cookies[i].getName();
                if (cname.equals(IAASConstants.SESSION_GROUPID_KEY)){ 
                    return cookies[i].getValue();
                }
            }
        }

        return null;
    }
    
    
    static public String getCookieSessionID(){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext excontext = context.getExternalContext();
        HttpServletRequest httpServletRequest = (HttpServletRequest)excontext.getRequest();

        Cookie[] cookies = httpServletRequest.getCookies();

        if (cookies != null) {
            for(int i=0; i<cookies.length; i++){
                String cname = cookies[i].getName();
                if (cname.equals(IAASConstants.SESSION_ID_KEY)){ 
                    return cookies[i].getValue();
                }
            }
        }

        return null;
    }
    
    static public String getCookieGroupTyp(){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext excontext = context.getExternalContext();
        HttpServletRequest httpServletRequest = (HttpServletRequest)excontext.getRequest();

        Cookie[] cookies = httpServletRequest.getCookies();

        if (cookies != null) {
            for(int i=0; i<cookies.length; i++){
                String cname = cookies[i].getName();
                if (cname.equals(IAASConstants.SESSION_GROUPTYPE_KEY)){ 
                    return cookies[i].getValue();
                }
            }
        }

        return null;
    }
    
    public static void deleteCookies(){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext excontext = context.getExternalContext();
	HttpServletResponse httpServletResponse = (HttpServletResponse)excontext.getResponse();
        
        Cookie cookie = new Cookie(IAASConstants.SESSION_USERNAME_KEY, null);  
        cookie.setPath("/");
        cookie.setMaxAge(0);
        httpServletResponse.addCookie(cookie);
        
        cookie = new Cookie(IAASConstants.SESSION_ID_KEY, null);  
        cookie.setPath("/");
        cookie.setMaxAge(0);
        httpServletResponse.addCookie(cookie);
        
        cookie = new Cookie(IAASConstants.SESSION_GROUPTYPE_KEY, null);  
        cookie.setPath("/");
        cookie.setMaxAge(0);
        httpServletResponse.addCookie(cookie);
        
        cookie = new Cookie(IAASConstants.SESSION_GROUPID_KEY, null);  
        cookie.setPath("/");
        cookie.setMaxAge(0);
        httpServletResponse.addCookie(cookie);
    }
    
    public static void deleteServletSession(){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext excontext = context.getExternalContext();
	HttpServletRequest httpServletReq = (HttpServletRequest)excontext.getRequest();
        httpServletReq.getSession().invalidate();
    }
}
