package app.sikd.aas;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 *
 * @author Sora
 */
public class DefaultExceptionHandlerFactory extends ExceptionHandlerFactory{

    private final ExceptionHandlerFactory parent;
    
    public DefaultExceptionHandlerFactory(ExceptionHandlerFactory parent) {
	        this.parent = parent;
	    }
    
    @Override
    public ExceptionHandler getExceptionHandler() {               
        ExceptionHandler eh = parent.getExceptionHandler();
	        eh = new DefaultExceptionHandler(eh);
	 
	        return eh;
    }

}
