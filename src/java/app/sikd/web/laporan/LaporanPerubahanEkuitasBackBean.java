/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan;

import app.sikd.web.common.bean.ABasicFilteredBackBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ViewScoped
@ManagedBean(name = "lpEkuitasBean")
public class LaporanPerubahanEkuitasBackBean extends ABasicFilteredBackBean {

    private PerubahanEkuitasReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new PerubahanEkuitasReportWorker();
        setPage("/Laporan/lpekuitas/lpekuitas-content.xhtml");
    }

    private List<EkuitasObj> reports;

    public List<EkuitasObj> getData() {
        return reports;
    }

    private void onChangeProcess() {
        try {
            reports = reportWorker.updatePerubahanEkuitasReport(new Short(year), pemda);
        } catch (Exception ex) {
            Logger.getLogger(NeracaBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onFilterChanged() {
        if (pemda != null && (year != null && year.length() > 0)) {
            onChangeProcess();
        }
    }
}
