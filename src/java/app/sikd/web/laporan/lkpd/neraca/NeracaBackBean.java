/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.lkpd.neraca;

import app.sikd.web.common.ASinglePageBackbean;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Nongky
 */
@ViewScoped
@ManagedBean(name = "lkpdNeracaBean")
public class NeracaBackBean extends ASinglePageBackbean {

    private List<NeracaStatus> data;

    @PostConstruct
    private void init() {
        setPage("/Laporan/LKPD/neraca/neraca-content.xhtml");
        data = new ArrayList<>();
        data.add(new NeracaStatus("APBD Murni", "Belum Mengirimkan data"));
        data.add(new NeracaStatus("APBD Perubahan", "Belum Mengirimkan data"));
        data.add(new NeracaStatus("lra bulan 1", "Belum Mengirimkan data"));
        data.add(new NeracaStatus("lra bulan 2", "Belum Mengirimkan data"));
        data.add(new NeracaStatus("lra bulan 3", "Belum Mengirimkan data"));
        data.add(new NeracaStatus("lra bulan 4", "Belum Mengirimkan data"));
        data.add(new NeracaStatus("lra bulan 5", "Belum Mengirimkan data"));
        data.add(new NeracaStatus("lra bulan 6", "Belum Mengirimkan data"));
        data.add(new NeracaStatus("lra bulan 7", "Belum Mengirimkan data"));
        data.add(new NeracaStatus("lra bulan 8", "Belum Mengirimkan data"));
        data.add(new NeracaStatus("lra bulan 9", "Belum Mengirimkan data"));
        data.add(new NeracaStatus("lra bulan 10", "Belum Mengirimkan data"));
        data.add(new NeracaStatus("lra bulan 11", "Belum Mengirimkan data"));
        data.add(new NeracaStatus("lra bulan 12", "Belum Mengirimkan data"));

    }

    public List<NeracaStatus> getData() {
        return data;
    }

    public class NeracaStatus {

        String name;
        String status;

        public NeracaStatus(String name, String status) {
            this.name = name;
            this.status = status;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }

}
