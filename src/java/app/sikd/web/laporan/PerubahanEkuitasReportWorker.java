/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.backoffice.PerubahanEkuitas;
import app.sikd.office.ejb.session.LaporanSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class PerubahanEkuitasReportWorker {

    private LaporanSessionBeanRemote service;

    public PerubahanEkuitas selectedEkuitas, editedEkuitas;

    public PerubahanEkuitasReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (LaporanSessionBeanRemote) context.lookup("LaporanSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(PerubahanEkuitasReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public PerubahanEkuitas getSelectedEkuitas() {
        return selectedEkuitas;
    }

    public void setSelectedEkuitas(PerubahanEkuitas selectedEkuitas) {
        this.selectedEkuitas = selectedEkuitas;
    }

    public PerubahanEkuitas getEditedEkuitas() {
        return editedEkuitas;
    }

    public void setEditedEkuitas(PerubahanEkuitas editedEkuitas) {
        this.editedEkuitas = editedEkuitas;
    }

    private List<EkuitasObj> ekuitas = new ArrayList();

    public List<EkuitasObj> updatePerubahanEkuitasReport(short _year, Pemda _pemda) throws Exception {
        ekuitas.clear();
        selectedEkuitas = service.getPerubahanEkuitasReports(_year, _pemda.getKodeSatker());

        if (selectedEkuitas == null) {
            selectedEkuitas = new PerubahanEkuitas();
            selectedEkuitas.setTahunAnggaran(_year);
            selectedEkuitas.setKodeSatker(_pemda.getKodeSatker());
        }
        editedEkuitas = new PerubahanEkuitas(selectedEkuitas.getIndex(), selectedEkuitas.getKodeSatker(), selectedEkuitas.getKodePemda(), selectedEkuitas.getNamaPemda(), selectedEkuitas.getTahunAnggaran(), selectedEkuitas.getEkuitasAwal(), selectedEkuitas.getSurplusDefisitLO(), selectedEkuitas.getKoreksiNilaiPersediaan(), selectedEkuitas.getSelisihRevaluasiAset(), selectedEkuitas.getLainLain());

        if (editedEkuitas.getIndex() <= 0) {
            editedEkuitas.setKodePemda(_pemda.getKodePemda());
            editedEkuitas.setNamaPemda(_pemda.getNamaPemda());
        }

        PerubahanEkuitas eMin = service.getPerubahanEkuitasReports((short) (_year - 1), _pemda.getKodeSatker());

        if (selectedEkuitas == null) {
            selectedEkuitas = new PerubahanEkuitas();
        }
        if (eMin == null) {
            eMin = new PerubahanEkuitas();
        }
        double tot = selectedEkuitas.getEkuitasAwal() + selectedEkuitas.getSurplusDefisitLO() + selectedEkuitas.getKoreksiNilaiPersediaan() + selectedEkuitas.getSelisihRevaluasiAset() + selectedEkuitas.getLainLain();
        double totMin = eMin.getEkuitasAwal() + eMin.getSurplusDefisitLO() + eMin.getKoreksiNilaiPersediaan() + eMin.getSelisihRevaluasiAset() + eMin.getLainLain();

        ekuitas.add(new EkuitasObj("1", "Ekuitas Awal", (selectedEkuitas.getIndex() > 0) ? selectedEkuitas.getEkuitasAwalAsString() : "",
                (eMin.getIndex() > 0) ? eMin.getEkuitasAwalAsString() : "", ""));
        ekuitas.add(new EkuitasObj("2", "Surplus/Defisit - LO", (selectedEkuitas.getIndex() > 0) ? selectedEkuitas.getSurplusDefisitLOAsString() : "", (eMin.getIndex() > 0) ? eMin.getSurplusDefisitLOAsString() : "", ""));
        ekuitas.add(new EkuitasObj("", "Dampak Kumulatif Perubahan Kebijakan/Kesalahan Mendasar :", "", "", ""));
        ekuitas.add(new EkuitasObj("3", "Koreksi Nilai Persediaan", (selectedEkuitas.getIndex() > 0) ? selectedEkuitas.getKoreksiNilaiPersediaanAsString() : "", (eMin.getIndex() > 0) ? eMin.getKoreksiNilaiPersediaanAsString() : "", ""));
        ekuitas.add(new EkuitasObj("4", "Selisih Revaluasi Aset Tetap", (selectedEkuitas.getIndex() > 0) ? selectedEkuitas.getSelisihRevaluasiAsetAsString() : "", (eMin.getIndex() > 0) ? eMin.getSelisihRevaluasiAsetAsString() : "", ""));
        ekuitas.add(new EkuitasObj("5", "Lain-lain", (selectedEkuitas.getIndex() > 0) ? selectedEkuitas.getLainLainAsString() : "", (eMin.getIndex() > 0) ? eMin.getLainLainAsString() : "", ""));
        ekuitas.add(new EkuitasObj("", "Ekuitas Akhir", (selectedEkuitas.getIndex() > 0) ? SIKDUtil.doubleToString(tot) : "", (eMin.getIndex() > 0) ? SIKDUtil.doubleToString(totMin) : "", "font-weight: bold; background-color: activecaption;"));

        return ekuitas;
    }

}
