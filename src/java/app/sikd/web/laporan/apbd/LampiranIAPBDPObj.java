/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.laporan.apbd;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 *
 * @author Detra
 */
public class LampiranIAPBDPObj implements Serializable{
    String nourut;
    String uraian;
//    String murni;
//    String perubahan;
    double dmurni;
    double dperubahan;
    
//    String surplus;
//    String persen;
    
    String fontStyle;

    public LampiranIAPBDPObj() { 
    }

    public LampiranIAPBDPObj(String nourut, String uraian, double dmurni, double dperubahan) {
        this.nourut = nourut;
        this.uraian = uraian;        
        this.dmurni = dmurni;
        this.dperubahan = dperubahan;
//        this.surplus = surplus;
//        this.persen = persen;
    }
    
    public String getNourut() {
          if( nourut!=null && !nourut.trim().equals("")){
            if( nourut.trim().startsWith("5") ) this.nourut = "2"+nourut.substring(1).trim();
            else if( nourut.trim().startsWith("4") ) this.nourut= "1"+nourut.substring(1).trim();       
            else if( nourut.trim().startsWith("6") ) this.nourut = "3"+nourut.substring(1).trim();           
        }
        return nourut;
    }

    public void setNourut(String nourut) {
        this.nourut = nourut;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public String getFontStyle() {
        if(fontStyle==null || fontStyle.trim().equals("")) fontStyle="font-weight: normal;";
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }
    
    String getJumlahNya(double jj){
        Locale.setDefault(Locale.GERMAN);
        DecimalFormat df = new DecimalFormat("#,###,##0.00");
        df.setNegativePrefix("( ");
        df.setNegativeSuffix(" )");
        String result = df.format(jj);
        return result;
    }

    public String getMurni() {
        if(nourut!=null && !nourut.trim().equals("")&&nourut.trim().length()==1) return "";
        else if((nourut==null || nourut.trim().equals(""))&&(uraian==null||uraian.trim().equals(""))) return "";
        return getJumlahNya(dmurni);
    }

    public void setMurni(String murni) {
//        this.murni = murni;
    }

    public String getPerubahan() {
        if(nourut!=null && !nourut.trim().equals("")&&nourut.trim().length()==1) return "";
        else if((nourut==null || nourut.trim().equals(""))&&(uraian==null||uraian.trim().equals(""))) return "";
        return getJumlahNya(dperubahan);
//        return perubahan;
    }

    public void setPerubahan(String perubahan) {
//        this.perubahan = perubahan;
    }

    public String getSurplus() {
        double dd = dperubahan-dmurni;
        if(nourut!=null && !nourut.trim().equals("")&&nourut.trim().length()==1) return "";
        else if((nourut==null || nourut.trim().equals(""))&&(uraian==null||uraian.trim().equals(""))) return "";
        return getJumlahNya(dd);
    }

    public void setSurplus(String surplus) {
//        this.surplus = surplus;
    }

    public String getPersen() {
        double dd = Math.abs(dperubahan/dmurni)*100;
//        dd = (dd/dperubahan) * 100;
        if(nourut!=null && !nourut.trim().equals("")&&nourut.trim().length()==1) return "";
        else if((nourut==null || nourut.trim().equals(""))&&(uraian==null||uraian.trim().equals(""))) return "";
        return getJumlahNya(dd)+" %";
    }

    public void setPersen(String persen) {
//        this.persen = persen;
    }

    public double getDmurni() {        
        return dmurni;
    }

    public void setDmurni(double dmurni) {
        this.dmurni = dmurni;
    }

    public double getDperubahan() {
        return dperubahan;
    }

    public void setDperubahan(double dperubahan) {
        this.dperubahan = dperubahan;
    }
    
}