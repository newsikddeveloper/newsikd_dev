/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.web.laporan.*;
import app.sikd.aas.ServerUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.apbd.LampiranIPerdaAPBD;
import app.sikd.entity.backoffice.APBD;
import app.sikd.entity.mgr.UserAccount;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Nongky
 */
public class LampiranIReportWorker {

    private APBDSessionBeanRemote service;
    private NewLoginSessionBeanRemote loginService;
    private List<LampiranIAPBDObj> apbds = new ArrayList<>();
    private APBD apbd;

    public LampiranIReportWorker() {
        try {
            Context loginContext = ServerUtil.getLoginContext();
            loginService = (NewLoginSessionBeanRemote) loginContext.lookup("NewLoginSessionBean/remote");

            Context context = ServerUtil.getOfficeContext();
            service = (APBDSessionBeanRemote) context.lookup("APBDSessionBean/remote");

        } catch (Exception ex) {
            Logger.getLogger(LampiranIReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranIPerdaAPBD> updateLampiranIReport(short _year, Pemda _pemda, short _coaType) throws Exception {

        
//       List<String> kodepemdas = loginService.getPemdaCodeUnders(_pemda.getKodeSatker());
//        
//        List<LampiranIPerdaAPBD> ls;
//        
//        
//        
//                ls = service.getKompilasiLampiranIPerdaAPBDMurni(_year, _pemda.getKodePemda(), _coaType);
//            } else {
//                APBDPerda per = apbdSession.getAPBDMurniPerdaNumber(yy, getKodeSatker(), jenisCOA);
//                if (per != null) {
//                    this.nomor = per.getNomor();
//                    this.tanggal = SIKDUtil.dateToString(per.getDate());
//                }
//                ls = apbdSession.getLampiranIPerdaAPBDMurni(yy, getKodeSatker(), jenisCOA);
////                setData(ls);
//            }
//        
//        if (jenisCOA == 13) {
//            setData13(apbds);
//        } else if (jenisCOA == 64) {
//            setData64(apbds);
//        }
        return null;
    }

    public void setData13(List<LampiranIPerdaAPBD> ls) {
        double jumlah4 = 0, jumlah5 = 0, jumlah61 = 0, jumlah62 = 0;
        String strSub4 = "", strSub5 = "", strSub61 = "", strSub62 = "";
        if (ls != null && !ls.isEmpty()) {
            for (LampiranIPerdaAPBD l : ls) {
                if (l.getKode().trim().equals("4")) {
                    jumlah4 = l.getJumlah();
                    strSub4 = "JUMLAH PENDAPATAN";
                } else if (l.getKode().trim().equals("5")) {
                    if (!strSub4.trim().equals("")) {
                        apbds.add(new LampiranIAPBDObj("", strSub4, SIKDUtil.doubleToString(jumlah4)));
                        apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                        apbds.add(new LampiranIAPBDObj("", "", ""));
                    }

                    jumlah5 = l.getJumlah();
                    strSub5 = "JUMLAH BELANJA";
                } else if (l.getKode().trim().equals("6")) {
                    if (!strSub5.trim().equals("")) {
                        apbds.add(new LampiranIAPBDObj("", strSub5, SIKDUtil.doubleToString(jumlah5)));
                        apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                        apbds.add(new LampiranIAPBDObj("", "", ""));
                    }
                    if (!strSub4.trim().equals("") || !strSub5.trim().equals("")) {
                        apbds.add(new LampiranIAPBDObj("", "SURPLUS/DEFISIT", SIKDUtil.doubleToString(jumlah4 - jumlah5)));
                        apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                        apbds.add(new LampiranIAPBDObj("", "", ""));
                    }
                } else if (l.getKode().trim().equals("6.1")) {
                    jumlah61 = l.getJumlah();
                    strSub61 = "Jumlah Penerimaan Pembiayaan";
                } else if (l.getKode().trim().equals("6.2")) {
                    if (!strSub61.trim().equals("")) {
                        apbds.add(new LampiranIAPBDObj("", strSub61, SIKDUtil.doubleToString(jumlah61)));
                        apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                        apbds.add(new LampiranIAPBDObj("", "", ""));
                    }
                    jumlah62 = l.getJumlah();
                    strSub62 = "Jumlah Pengeluaran Pembiayaan";
                }
                if (l.getKode().trim().length() == 1) {
                    apbds.add(new LampiranIAPBDObj(l.getKode(), l.getUraian(), ""));
                } else {
                    apbds.add(new LampiranIAPBDObj(l.getKode(), l.getUraian(), l.getJumlahString()));
                }
                if (l.getKode().trim().length() < 4) {
                    apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                }
            }
            if (!strSub62.trim().equals("")) {
                apbds.add(new LampiranIAPBDObj("", strSub62, SIKDUtil.doubleToString(jumlah62)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));

                apbds.add(new LampiranIAPBDObj("", "PEMBIAYAAN NETTO", SIKDUtil.doubleToString(jumlah61 - jumlah62)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));
            }
            if (strSub62.trim().equals("") && !strSub61.trim().equals("")) {
                apbds.add(new LampiranIAPBDObj("", strSub61, SIKDUtil.doubleToString(jumlah61)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));

                apbds.add(new LampiranIAPBDObj("", "PEMBIAYAAN NETTO", SIKDUtil.doubleToString(jumlah61 - jumlah62)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));
            }
            if (strSub62.trim().equals("") && strSub61.trim().equals("") && !strSub5.trim().equals("")) {
                apbds.add(new LampiranIAPBDObj("", strSub5, SIKDUtil.doubleToString(jumlah5)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));

                apbds.add(new LampiranIAPBDObj("", "SURPLUS/DEFISIT", SIKDUtil.doubleToString(jumlah4 - jumlah5)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));
            }
            if (strSub62.trim().equals("") && strSub61.trim().equals("") && strSub5.trim().equals("") && strSub4.trim().equals("")) {
                apbds.add(new LampiranIAPBDObj("", strSub4, SIKDUtil.doubleToString(jumlah4)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));

                apbds.add(new LampiranIAPBDObj("", "SURPLUS/DEFISIT", SIKDUtil.doubleToString(jumlah4 - jumlah5)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));
            }

            apbds.add(new LampiranIAPBDObj("", "Sisa Lebih Pembiayaan Anggaran Tahun Berkenaan (SILPA)", SIKDUtil.doubleToString((jumlah4 - jumlah5) + (jumlah61 - jumlah62))));
            apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
            apbds.add(new LampiranIAPBDObj("", "", ""));
        }

    }

    public void setData64(List<LampiranIPerdaAPBD> ls) {
        double jumlah4 = 0, jumlah5 = 0, jumlah6 = 0, jumlah71 = 0, jumlah72 = 0;
        String strSub4 = "", strSub5 = "", strSub6 = "", strSub71 = "", strSub72 = "";
        if (ls != null && !ls.isEmpty()) {
            for (LampiranIPerdaAPBD l : ls) {
                if (l.getKode().trim().equals("4")) {
                    jumlah4 = l.getJumlah();
                    strSub4 = "JUMLAH PENDAPATAN";
                } else if (l.getKode().trim().equals("5")) {
                    if (!strSub4.trim().equals("")) {
                        apbds.add(new LampiranIAPBDObj("", strSub4, SIKDUtil.doubleToString(jumlah4)));
                        apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                        apbds.add(new LampiranIAPBDObj("", "", ""));
                    }

                    jumlah5 = l.getJumlah();
                    strSub5 = "JUMLAH BELANJA";
                } else if (l.getKode().trim().equals("6")) {
                    if (!strSub4.trim().equals("")) {
                        apbds.add(new LampiranIAPBDObj("", strSub5, SIKDUtil.doubleToString(jumlah5)));
                        apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                        apbds.add(new LampiranIAPBDObj("", "", ""));
                    }

                    jumlah6 = l.getJumlah();
                    strSub6 = "JUMLAH TRANSFER";
                } else if (l.getKode().trim().equals("7")) {
                    if (!strSub6.trim().equals("")) {
                        apbds.add(new LampiranIAPBDObj("", strSub6, SIKDUtil.doubleToString(jumlah6)));
                        apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                        apbds.add(new LampiranIAPBDObj("", "", ""));
                    }
                    if (!strSub5.trim().equals("") || !strSub6.trim().equals("")) {
                        apbds.add(new LampiranIAPBDObj("", "JUMLAH BELANJA DAN TRANSFER", SIKDUtil.doubleToString(jumlah5 + jumlah6)));
                        apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                        apbds.add(new LampiranIAPBDObj("", "", ""));
                    }
                    if (!strSub4.trim().equals("") || !strSub5.trim().equals("") || !strSub6.trim().equals("")) {
                        apbds.add(new LampiranIAPBDObj("", "SURPLUS/DEFISIT", SIKDUtil.doubleToString(jumlah4 - (jumlah5 + jumlah6))));
                        apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                        apbds.add(new LampiranIAPBDObj("", "", ""));
                    }
                } else if (l.getKode().trim().equals("7.1")) {
                    jumlah71 = l.getJumlah();
                    strSub71 = "Jumlah Penerimaan Pembiayaan";
                } else if (l.getKode().trim().equals("7.2")) {
                    if (!strSub71.trim().equals("")) {
                        apbds.add(new LampiranIAPBDObj("", strSub71, SIKDUtil.doubleToString(jumlah71)));
                        apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                        apbds.add(new LampiranIAPBDObj("", "", ""));
                    }
                    jumlah72 = l.getJumlah();
                    strSub72 = "Jumlah Pengeluaran Pembiayaan";
                }
                if (l.getKode().trim().length() == 1) {
                    apbds.add(new LampiranIAPBDObj(l.getKode(), l.getUraian(), ""));
                } else {
                    apbds.add(new LampiranIAPBDObj(l.getKode(), l.getUraian(), l.getJumlahString()));
                }
                if (l.getKode().trim().length() < 4) {
                    apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                }
            }

            if (!strSub72.trim().equals("")) {
                apbds.add(new LampiranIAPBDObj("", strSub72, SIKDUtil.doubleToString(jumlah72)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));

                apbds.add(new LampiranIAPBDObj("", "JUMLAH PEMBIAYAAN", SIKDUtil.doubleToString(jumlah71 - jumlah72)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));
            }
            if (strSub72.trim().equals("") && !strSub71.trim().equals("")) {
                apbds.add(new LampiranIAPBDObj("", strSub71, SIKDUtil.doubleToString(jumlah71)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));

                apbds.add(new LampiranIAPBDObj("", "JUMLAH PEMBIAYAAN", SIKDUtil.doubleToString(jumlah71 - jumlah72)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));
            }

            if (strSub72.trim().equals("") && strSub71.trim().equals("") && !strSub6.trim().equals("")) {
                apbds.add(new LampiranIAPBDObj("", strSub6, SIKDUtil.doubleToString(jumlah6)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));

                apbds.add(new LampiranIAPBDObj("", "JUMLAH BELANJA DAN TRANSFER", SIKDUtil.doubleToString(jumlah5 + jumlah6)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));

                apbds.add(new LampiranIAPBDObj("", "SURPLUS/DEFISIT", SIKDUtil.doubleToString(jumlah4 - (jumlah5 + jumlah6))));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));
            }

            if (strSub72.trim().equals("") && strSub71.trim().equals("") && strSub6.trim().equals("") && !strSub5.trim().equals("")) {
                apbds.add(new LampiranIAPBDObj("", strSub5, SIKDUtil.doubleToString(jumlah5)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));

                apbds.add(new LampiranIAPBDObj("", "JUMLAH BELANJA DAN TRANSFER", SIKDUtil.doubleToString(jumlah5 + jumlah6)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));

                apbds.add(new LampiranIAPBDObj("", "SURPLUS/DEFISIT", SIKDUtil.doubleToString(jumlah4 - (jumlah5 + jumlah6))));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));
            }

            if (strSub72.trim().equals("") && strSub71.trim().equals("") && strSub6.trim().equals("") && strSub5.trim().equals("") && !strSub4.trim().equals("")) {
                apbds.add(new LampiranIAPBDObj("", strSub4, SIKDUtil.doubleToString(jumlah4)));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));

                apbds.add(new LampiranIAPBDObj("", "SURPLUS/DEFISIT", SIKDUtil.doubleToString(jumlah4 - (jumlah5 + jumlah6))));
                apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
                apbds.add(new LampiranIAPBDObj("", "", ""));
            }

            apbds.add(new LampiranIAPBDObj("", "SISA LEBIH PEMBIAYAAN ANGGARAN/(SISA KURANG PEMBIAYAAN ANGGARAN)", SIKDUtil.doubleToString((jumlah4 - (jumlah5 + jumlah6) + (jumlah71 - jumlah72)))));
            apbds.get(apbds.size() - 1).setFontStyle("font-weight: bold;");
            apbds.add(new LampiranIAPBDObj("", "", ""));
        }

    }

}
