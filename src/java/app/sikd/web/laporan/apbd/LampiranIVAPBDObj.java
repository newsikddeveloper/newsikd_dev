/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.laporan.apbd;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class LampiranIVAPBDObj implements Serializable{
    private String kode;
    private String uraian;
    private String pegawai;
    private String barangJasa;
    private String modal;
    private String jumlah;
    private String fontStyle;
    

    public LampiranIVAPBDObj() {
    }

    public LampiranIVAPBDObj(String kode, String uraian, String pegawai, String barangJasa, String modal, String jumlah) {
        this.kode = kode;
        this.uraian = uraian;
        this.pegawai = pegawai;
        this.barangJasa = barangJasa;
        this.modal = modal;
        this.jumlah = jumlah;
    }

    public LampiranIVAPBDObj(String kode, String uraian, String pegawai, String barangJasa, String modal, String jumlah, String fontStyle) {
        this.kode = kode;
        this.uraian = uraian;
        this.pegawai = pegawai;
        this.barangJasa = barangJasa;
        this.modal = modal;
        this.jumlah = jumlah;
        this.fontStyle = fontStyle;
    }

    public String getJumlah() {
        if(kode == null || kode.trim().equals("") || kode.trim().length() < 10 ) return "";
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public String getPegawai() {
        if(kode == null || kode.trim().equals("") || kode.trim().length() < 10 ) return "";
        return pegawai;
    }

    public void setPegawai(String pegawai) {
        this.pegawai = pegawai;
    }

    public String getBarangJasa() {
        if(kode == null || kode.trim().equals("") || kode.trim().length() < 10 ) return "";
        return barangJasa;
    }

    public void setBarangJasa(String barangJasa) {
        this.barangJasa = barangJasa;
    }

    public String getModal() {
        if(kode == null || kode.trim().equals("") || kode.trim().length() < 10 ) return "";
        return modal;
    }

    public void setModal(String modal) {
        this.modal = modal;
    }

    public String getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }
    
    
}
