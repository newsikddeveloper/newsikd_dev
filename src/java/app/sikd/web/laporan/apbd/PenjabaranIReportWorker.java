/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.apbd.LampiranIPerdaAPBD;
import app.sikd.office.ejb.session.APBDSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class PenjabaranIReportWorker implements Serializable{

    private APBDSessionBeanRemote service;
    private List<LampiranIAPBDObj> apbds = new ArrayList<>();
    private short coaType = SIKDUtil.COA_PMDN13;

    public PenjabaranIReportWorker() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (APBDSessionBeanRemote) context.lookup("APBDSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(PenjabaranIReportWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranIAPBDObj> updatePenjabaranLampiranIReport(short _year, Pemda _pemda) throws Exception {

        apbds.clear();
        List<LampiranIPerdaAPBD> ls = service.getLampiranIPenjabaranAPBDbyYear(_year, _pemda.getKodeSatker(), (short) 0, coaType);
        int iii = 0;
        for (LampiranIPerdaAPBD l : ls) {
            apbds.add(new LampiranIAPBDObj(l.getKode(), l.getUraian(), l.getJumlahString()));
            if (l.getKode().trim().length() < 10) {
                apbds.get(iii).setFontStyle("font-weight: bold;");
            }
            iii++;
        }
        return apbds;
    }

}
