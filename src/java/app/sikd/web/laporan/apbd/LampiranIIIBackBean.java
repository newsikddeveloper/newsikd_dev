/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.entity.apbd.LampiranIPerdaAPBD;
import app.sikd.util.SIKDUtil;
import app.sikd.web.common.bean.ABasicFilteredBackBean;
import app.sikd.web.common.bean.ISKPDFilteredBean;
import app.sikd.web.common.filter.SKPDFilter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "apbdLampiranIII")
@ViewScoped
public class LampiranIIIBackBean extends ABasicFilteredBackBean implements ISKPDFilteredBean {

    private LampiranIReportWorker reportWorker;
    private String coaType;
    private SKPDFilter skpdFilter;
    private String skpd;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        additionalRender = ":data-filter-form:skpd-label-som";
        reportWorker = new LampiranIReportWorker();
        skpdFilter = new SKPDFilter();
        setPage("/Laporan/apbd/lampiran3/lampiran3-content.xhtml");
    }

    private List<LampiranIPerdaAPBD> reports;

    public void onChangeProcess() {
        try {
            reports = reportWorker.updateLampiranIReport(new Short(year), pemda, new Short(coaType));
        } catch (Exception ex) {
            Logger.getLogger(LampiranIIIBackBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<LampiranIPerdaAPBD> getData() {
        return reports;
    }

    @Override
    public void onFilterChanged() {
        if (skpdList != null) {
            skpdList.clear();
        }
        if (pemda != null && (year != null && year.length() > 0)) {
            try {
                skpdList = skpdFilter.getSKPDs(new Short(year), pemda.getKodeSatker(), SIKDUtil.APBD_MURNI_SHORT, (short) 1);
            } catch (Exception ex) {
                Logger.getLogger(LampiranIIIBackBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private List<String> skpdList;

    @Override
    public List<String> getSKPDs() {
        return skpdList;
    }

    @Override
    public void setSelectedSKPD(String _skpd) {
        skpd = _skpd;
    }

    @Override
    public String getSelectedSKPD() {
        return skpd;
    }

}
