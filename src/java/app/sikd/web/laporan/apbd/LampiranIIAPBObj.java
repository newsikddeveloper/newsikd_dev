/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import java.io.Serializable;

/**
 *
 * @author PC04
 */
public class LampiranIIAPBObj implements Serializable {
    
    String kode;
    String urusanpemerin;
    String pendapatan;
    String btl;
    String bl;
    String jumbel;

    public LampiranIIAPBObj() {
    }

    public String getBl() {
        return bl;
    }

    public void setBl(String bl) {
        this.bl = bl;
    }

    public String getBtl() {
        return btl;
    }

    public void setBtl(String btl) {
        this.btl = btl;
    }

    public String getJumbel() {
        return jumbel;
    }

    public void setJumbel(String jumbel) {
        this.jumbel = jumbel;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPendapatan() {
        return pendapatan;
    }

    public void setPendapatan(String pendapatan) {
        this.pendapatan = pendapatan;
    }

    public String getUrusanpemerin() {
        return urusanpemerin;
    }

    public void setUrusanpemerin(String urusanpemerin) {
        this.urusanpemerin = urusanpemerin;
    }
}
