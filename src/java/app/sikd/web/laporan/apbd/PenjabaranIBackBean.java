/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.laporan.apbd;

import app.sikd.web.common.bean.ACOAFilteredBackBean;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ManagedBean(name = "penjabaranI")
@ViewScoped
public class PenjabaranIBackBean extends ACOAFilteredBackBean {

    private PenjabaranIReportWorker reportWorker;

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        reportWorker = new PenjabaranIReportWorker();
        setPage("/Laporan/apbd/penjabaran1/lampiran1-content.xhtml");
    }

    private List<LampiranIAPBDObj> reports;

    @Override
    protected void onChangeProcess() throws Exception {
        reports = reportWorker.updatePenjabaranLampiranIReport(new Short(year), pemda);
    }

    public List<LampiranIAPBDObj> getData() {
        return reports;
    }

}
