/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.laporan.apbd;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 *
 * @author sora
 */
public class RealisasiAPBDObj implements Serializable{
    String no;
    String uraian;
    String anggaran;
    double anggaranDouble;
    String realisasi;
    String lebihKurang;
    String persen;
    String fontStyle;

    public RealisasiAPBDObj() {
    }

    public RealisasiAPBDObj(String no, String uraian, String anggaran, String realisasi, String lebihKurang, String persen) {
        this.no = no;
        this.uraian = uraian;
        this.anggaran = anggaran;
        this.realisasi = realisasi;
        this.lebihKurang = lebihKurang;
        this.persen = persen;
    }
    
    public RealisasiAPBDObj(String no, String uraian, double anggaranDouble, String realisasi, String lebihKurang, String persen) {
        this.no = no;
        this.uraian = uraian;
        this.anggaranDouble = anggaranDouble;
        this.anggaran = getJumlahNya(anggaranDouble);
        this.realisasi = realisasi;
        this.lebihKurang = lebihKurang;
        this.persen = persen;
    }

    
    public String getNo() {
        if( no!=null && !no.trim().equals("")){
            if( no.trim().startsWith("5") ) this.no = "2"+no.substring(1).trim();
            else if( no.trim().startsWith("4") ) this.no= "1"+no.substring(1).trim();       
            else if( no.trim().startsWith("6") ) this.no = "3"+no.substring(1).trim();           
        }
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public String getAnggaran() {
      
        return anggaran;
    }
    
    public void setAnggaran(String anggaran) {
        this.anggaran = anggaran;
    }

    public String getRealisasi() {
        return realisasi;
    }

    public void setRealisasi(String realisasi) {
        this.realisasi = realisasi;
    }

    public String getLebihKurang() {
        return lebihKurang;
    }

    public void setLebihKurang(String lebihKurang) {
        this.lebihKurang = lebihKurang;
    }

    public String getPersen() {
        return persen;
    }

    public void setPersen(String persen) {
        this.persen = persen;
    }

    public String getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }

    public double getAnggaranDouble() {
        return anggaranDouble;
    }

    public void setAnggaranDouble(double anggaranDouble) {
        this.anggaranDouble = anggaranDouble;
        this.anggaran = getJumlahNya(anggaranDouble);
    }
    
    static String getJumlahNya(double jj){
        Locale.setDefault(Locale.GERMAN);
        DecimalFormat df = new DecimalFormat("#,###,##0.00");
        df.setNegativePrefix("( ");
        df.setNegativeSuffix(" )");
        String result = df.format(jj);
        return result;
    }
    
}
