package app.sikd.web;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.mgr.TMenu;
import app.sikd.util.SIKDUtil;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.MenuActionEvent;

/**
 *
 * @author detra
 */
@ManagedBean(name="klikMenuMBean") 
@RequestScoped 
public class KlikMenuMBean {
    

    public void click(ActionEvent e){        
        Context ctxLogin; 
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext excontext = fc.getExternalContext();
            HttpServletRequest req = (HttpServletRequest) excontext.getRequest();

            MenuActionEvent mm = (MenuActionEvent) e;
            MyMenuItem dmi = (MyMenuItem) mm.getMenuItem();
            excontext.redirect(dmi.getTitle());
        } catch (IOException ex) {
            logout();
            Logger.getLogger(KlikMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(KlikMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void klikAksi(TMenu m){
        Context ctxLogin; 
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext excontext = fc.getExternalContext();
            HttpServletRequest req = (HttpServletRequest) excontext.getRequest();
            if( m.getMasterMenu()!=null && m.getMasterMenu().getAlamat()!=null)
                excontext.redirect(m.getMasterMenu().getAlamat());
        } catch (IOException ex) {
            logout();
            Logger.getLogger(KlikMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(KlikMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
    
    public void goHome(){
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext excontext = fc.getExternalContext();
            HttpServletRequest req = (HttpServletRequest) excontext.getRequest();
            excontext.redirect(ServerUtil.getDomain() + excontext.getRequestContextPath() + "/home.jsf");
        } catch (IOException ex) {
            Logger.getLogger(KlikMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void goGroup(){
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext excontext = fc.getExternalContext();
            HttpServletRequest req = (HttpServletRequest) excontext.getRequest();
            excontext.redirect(ServerUtil.getDomain() + excontext.getRequestContextPath() + "/Pengaturan/AdministrasiPengguna/Group.jsf");
        } catch (IOException ex) {
            Logger.getLogger(KlikMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void goHakAkses(){
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext excontext = fc.getExternalContext();
            HttpServletRequest req = (HttpServletRequest) excontext.getRequest();
            excontext.redirect(ServerUtil.getDomain() + excontext.getRequestContextPath() + "/Pengaturan/AdministrasiPengguna/HakAkses.jsf");
        } catch (IOException ex) {
            Logger.getLogger(KlikMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void goWilayahKerja(){
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext excontext = fc.getExternalContext();
            HttpServletRequest req = (HttpServletRequest) excontext.getRequest();
            excontext.redirect(ServerUtil.getDomain() + excontext.getRequestContextPath() + "/Pengaturan/AdministrasiPengguna/WilayahKerja.jsf");
        } catch (IOException ex) {
            Logger.getLogger(KlikMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    public void logout() {
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext excontext = fc.getExternalContext();
            HttpServletRequest request = (HttpServletRequest) excontext.getRequest();
            request.getSession().removeAttribute(SIKDUtil.SESSION_USE);
            request.getSession().removeAttribute(SIKDUtil.SESSION_MEN);
            excontext.invalidateSession();
            excontext.redirect(ServerUtil.getDomain() + excontext.getRequestContextPath() + "/index.jsf");
//            System.out.println("request " + request.getSession().getAttribute(SIKDUtil.SESSION_USE));
        } catch (IOException ex) {
            Logger.getLogger(KlikMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    

}