package app.sikd.web;

import app.sikd.aas.ServerUtil;
import app.sikd.entity.mgr.TMenu;
import app.sikd.entity.mgr.TUserGroup;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSeparator;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author sora
 */
@ManagedBean(name = "menu3MBean")
@SessionScoped
public class Menu3MBean implements Serializable{
    private TUserGroup userGroup;
    private MenuModel model;

    @PostConstruct
    public void init() {
        model = new DefaultMenuModel();
    }
    
    public void createMenu(List<TMenu> menus){
        model = new DefaultMenuModel();
        @SuppressWarnings("UseOfObsoleteCollectionType")
        java.util.Vector vr = new java.util.Vector();
        @SuppressWarnings("UseOfObsoleteCollectionType")
        java.util.Vector vm = new java.util.Vector();
        String dom = ServerUtil.getDomain();
        if (menus != null && menus.size() > 0) {            
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext excontext = facesContext.getExternalContext();
            for (TMenu meu : menus) {
                
                if(meu.getParentMenu()!=null && meu.getParentMenu().getId()> 0 ){
                    int idP = getParent(vr, vm, meu.getParentMenu().getId());
                    DefaultSubMenu par = new DefaultSubMenu();
                    par.setLabel(((DefaultSubMenu)vm.get(idP)).getLabel());
                    par.setElements(((DefaultSubMenu)vm.get(idP)).getElements());
                    par.setStyle("font-size: 10px;");
                    par.setIcon(((DefaultSubMenu)vm.get(idP)).getIcon());
                    
                    if( meu.isMenuItem() ){
                        if( getMenuRight(meu) ){
                            if(!meu.getNama().trim().equalsIgnoreCase("separator")){
                            MyMenuItem item = new MyMenuItem(meu.getNama().trim());
//                            item.setIcon(meu.getIcon());
                            item.setCommand("#{klikMenuMBean.click()}");
                            item.setTitle(dom + excontext.getRequestContextPath()+"/" + meu.getMasterMenu().getAlamat());
                            item.setStyle("font-size: 10px;");
                            item.setGlobal(true);
                            item.setDisabled(!meu.getMasterMenu().isActives());
//                            item.setWrite(gr.isWrites());
                            par.addElement(item);
                            vm.setElementAt(par, idP);
                            }
                            else par.addElement( new DefaultSeparator());
                        }
                    }
                    else{
                        DefaultSubMenu dsm = new DefaultSubMenu(meu.getNama().trim());
                        dsm.setStyle("font-size: 10px;");
//                        dsm.setIcon(meu.getIcon());
                        par.addElement(dsm);
                        vm.setElementAt(par, idP);
                        vm.addElement(dsm);
                        vr.addElement(meu);
                    }
                }
                else{
                    if( meu.isMenuItem() ){
                        
                        if (getMenuRight(meu)) {
                            MyMenuItem mmi = new MyMenuItem(meu.getNama().trim());
                            mmi.setTitle(dom + excontext.getRequestContextPath() + "/" + meu.getMasterMenu().getAlamat());
                            mmi.setStyle("font-size: 10px;");
                            mmi.setGlobal(true);
                            mmi.setDisabled(!meu.getMasterMenu().isActives());
//                            mmi.setIcon(meu.getIcon());
                            mmi.setCommand("#{klikMenuMBean.click()}");
                            vr.addElement(meu);
                            vm.addElement(mmi);
                        }
                    }
                    else {
                        DefaultSubMenu rootMenu = new DefaultSubMenu(meu.getNama().trim());
                        rootMenu.setStyle("font-size: 10px;");
                        rootMenu.setIcon(meu.getIcon());
                        vr.addElement(meu);
                        vm.addElement(rootMenu);
                        
                    }
                }
            }
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext excontext = fc.getExternalContext();
        HttpServletRequest req = (HttpServletRequest) excontext.getRequest();

        for (int i = 0; i < vm.size(); i++) {            
            TMenu dsm = (TMenu) vr.get(i);
            if( dsm.getParentMenu().getId() <= 0 ){
                if (vm.get(i) instanceof DefaultSubMenu) {
                    DefaultSubMenu m = (DefaultSubMenu) vm.get(i);
                    m.setStyle("font-size: 10px;");
                    model.addElement(m);
                }
                else if(vm.get(i) instanceof MyMenuItem ){
                    model.addElement((MyMenuItem)vm.get(i));
                }
            }
            
        }

    }
    
    boolean getMenuRight(TMenu mn){
        boolean result = false;
        boolean find = false;
        if( userGroup!= null && userGroup.getRights()!= null ){
            int i = 0;
            while(i<userGroup.getRights().size() && find ==false ){
                if( userGroup.getRights().get(i).getMenu()!=null){                    
                    if( mn.getMasterMenu().getId() == userGroup.getRights().get(i).getMenu().getId()){
                            result = userGroup.getRights().get(i).isReads();
                        find = true;
                    }
                }
                    i++;
            }
        }
        return result;
    }
    
    int getParent(@SuppressWarnings("UseOfObsoleteCollectionType") java.util.Vector vr, @SuppressWarnings("UseOfObsoleteCollectionType") java.util.Vector vm, long id){
        int result = -1;
        if(vr.size() > 0 ){
            int i = 0;
            boolean find = false;
            while(i < vr.size() && find == false ){
                TMenu obj = (TMenu)vr.elementAt(i);
                if( obj.getId()== id ){
                    find = true;
                    result = i;
                }
                i++;
                
            }
        }
        return result;
    }

    public TUserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(TUserGroup userGroup) {
        this.userGroup = userGroup;
    }

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }
}