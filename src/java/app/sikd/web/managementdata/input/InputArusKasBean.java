/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.managementdata.input;

import app.sikd.web.common.bean.AArusKasBackBean;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ViewScoped
@ManagedBean(name = "inputArusKasBean")
public class InputArusKasBean extends AArusKasBackBean {

    @PostConstruct
    @Override
    protected void init() {
        super.init();
        setPage("/ManajemenData/InputData/aruskas/aruskas-content.xhtml");
    }

    @Override
    public void onChangeProcess() {
    }

    public List getData() {
        return null;
    }
}
