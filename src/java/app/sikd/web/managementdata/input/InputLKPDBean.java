/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.managementdata.input;

import app.sikd.entity.Pemda;
import app.sikd.web.common.ASinglePageBackbean;
import app.sikd.web.common.bean.IPemdaFilteredBean;
import app.sikd.web.common.bean.IYearFilteredBean;
import app.sikd.web.common.filter.PemdaFilter;
import app.sikd.web.common.filter.YearFilter;
import app.sikd.web.common.listener.IFilterChangedListener;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Nongky
 */
@ViewScoped
@ManagedBean(name = "inputLKPDBean")
public class InputLKPDBean extends ASinglePageBackbean implements IPemdaFilteredBean, IYearFilteredBean, IFilterChangedListener {

    private PemdaFilter pemdaFilter;
    private YearFilter yearFilter;

    private Pemda pemda;
    private String year;

    @PostConstruct
    private void init() {
        pemdaFilter = new PemdaFilter();
        yearFilter = new YearFilter();
        setPage("/ManajemenData/InputData/lkpd/lkpd-content.xhtml");
    }

    @Override
    public void onFilterChanged() {
        System.out.println("Filter Parameter :");
        System.out.println("selected pemda " + pemda.getKodePemda() + " " + pemda.getNamaPemda());
        System.out.println("selected year " + year);
    }

    @Override
    public List<Pemda> getPemdas() {
        return pemdaFilter.getPemdas();
    }

    @Override
    public List<String> getYears() {
        return yearFilter.getYears();
    }

    @Override
    public void setSelectedPemda(Pemda _pemda) {
        pemda = _pemda;
    }

    @Override
    public Pemda getSelectedPemda() {
        return pemda;
    }

    @Override
    public void setSelectedYear(String _year) {
        year = _year;
    }

    @Override
    public String getSelectedYear() {
        return year;
    }
    
    public List getData(){
        return null;
    }
}
