/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.setting;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.mgr.TUserGroup;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author donfebi
 */
@ManagedBean(name = "groupBean")
@SessionScoped
public class GroupBean implements Serializable {

    private TreeNode root;
    private TreeNode selectedNode;
    private String selectedGroupName;
//    private String selectedGroupDescription;
    private boolean elementGroupToCreate;
    private String newGroupDescription;
    private String newGroupName;
//    private String newGroupUse = "0";
    private boolean nodeHasChild;

    private String groupDescription;
    private String groupName;

    AdministrasiSessionBeanRemote administrasiSession;
    NewLoginSessionBeanRemote loginSession;
    long grupId;
    boolean writeAccess;

    @PostConstruct
    void init() {
        try {
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext excontext = context.getExternalContext();
        HttpServletRequest httpServletRequest = (HttpServletRequest)excontext.getRequest();
        String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }

            Context ctxLogin = ServerUtil.getLoginContext();
            administrasiSession = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            writeAccess = loginSession.canWrite(grupId, namamenu);
            root = new DefaultTreeNode("root", null);
            
            if (loginSession.canRead(grupId, namamenu)) {
                TUserGroup tug = administrasiSession.getUserGroup(grupId);
                TreeNode accnode = new DefaultTreeNode(tug, root);
                List<TUserGroup> subs = administrasiSession.getSubUserGroups(tug);
                tug.setSubGroups(subs);
                for (int i = 0; i < subs.size(); i++) {
                    TUserGroup get = subs.get(i);
                    TreeNode aa = new DefaultTreeNode(get, accnode);
                }
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam mengambil data User Group. "
                    + ex.toString()));
        }
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public boolean isElementGroupToCreate() {
        return elementGroupToCreate;
    }

    public void setElementGroupToCreate(boolean elementGroupToCreate) {
        this.elementGroupToCreate = elementGroupToCreate;
    }

    public String getNewGroupDescription() {
        return newGroupDescription;
    }

    public void setNewGroupDescription(String newGroupDescription) {
        this.newGroupDescription = newGroupDescription;
    }

    public String getNewGroupName() {
        return newGroupName;
    }

    public void setNewGroupName(String newGroupName) {
        this.newGroupName = newGroupName;
    }

    public String getSelectedGroupName() {
        return selectedGroupName;
    }

    public void setSelectedGroupName(String selectedGroupName) {
        this.selectedGroupName = selectedGroupName;
    }

    public boolean isNodeHasChild() {
        return nodeHasChild;
    }

    public void setNodeHasChild(boolean nodeHasChild) {
        this.nodeHasChild = nodeHasChild;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public void onNodeSelected(NodeSelectEvent event) {
        TreeNode node = event.getTreeNode();
        TUserGroup obj = (TUserGroup) node.getData();

        selectedGroupName = obj.getNama();

        if (obj.getSubGroups()==null) {
            try {
                List<TUserGroup> subs = administrasiSession.getSubUserGroups(obj);
                obj.setSubGroups(subs);

                for (TUserGroup su : subs) {
                        TreeNode accnode = new DefaultTreeNode(su, node);
                }

//                obj.setChildLoaded(true);
            } catch (Exception ex) {
                System.out.println("Error bro:" + ex);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Kegagalan",
                        "Gagal dalam mengambil data sub group. "
                        + ex.toString()));
            }
        }
    }

    public void onNodeExpanded(NodeExpandEvent event) {
        System.out.println("onNodeExpanded");
    }

    public void onNodeCollapsed(NodeCollapseEvent event) {
        System.out.println("onNodeCollapsed");
    }

    public void prepareNewGroup() {

        if (selectedNode == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Tidak ada User Group yang ditandai untuk dibuatkan sub"));
        }

    }
    
    TUserGroup getNewData() throws Exception{
        if( newGroupName== null || newGroupName.trim().equals(""))
            throw new Exception("Silahkan isi Nama User Group");
        TUserGroup newgrup = new TUserGroup(newGroupName, newGroupDescription);
        return newgrup;
    }

    public void createNewGroup() {
        try {
            String username = SessionUtil.getCookieUsername();

//            boolean access = loginSession.canWrite(grupId, groupName);
            if (!writeAccess) {
                throw new Exception("Anda Tidak Mempunyai Akses Menyimpan Data .");
            } else {
                TUserGroup parentGroup = (TUserGroup) selectedNode.getData();
                
                TUserGroup newgrup = getNewData();
                newgrup.setParentGroup(parentGroup);

                TUserGroup newg = administrasiSession.createUserGroup(newgrup);

                newgrup.setId(newg.getId());

                TreeNode newnode = new DefaultTreeNode(newgrup, selectedNode);

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Berhasil",
                        "Pembuatan UserGroup baru berhasil"));
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam membuat user group baru. "
                    + ex.toString()));
        }
    }

    public void prepareForEditingUserGroup() {
        TUserGroup grupnode = (TUserGroup) selectedNode.getData();
        groupName = grupnode.getNama();
        groupDescription = grupnode.getDeskripsi();
        
        newGroupName = grupnode.getNama();
        newGroupDescription = grupnode.getDeskripsi();
    }

    public void saveEditedGroup() {
        try {
            String username = SessionUtil.getCookieUsername();
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }

//            boolean access = loginSession.canWrite(grupId, "");
            if (!writeAccess) {
                throw new Exception("Anda Tidak Mempunyai Akses Menyimpan Data .");
            } else {
                TUserGroup gr = (TUserGroup) selectedNode.getData();
                TUserGroup newgr = getNewData();
                gr.setNama(newgr.getNama());
                gr.setDeskripsi(newgr.getDeskripsi());
                administrasiSession.updateUserGroup(gr);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Berhasil",
                        "Pembaharuan UserGroup berhasil"));
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam memperbaharui User Group"
                    + ex.toString()));
        }
    }

    public void prepareForDeletingUserGroup() {
        nodeHasChild = false;
        if (selectedNode.getChildCount() > 0) {
            nodeHasChild = true;
        }
    }

    public void deleteSelectedGroup() {
        try {
            String username = SessionUtil.getCookieUsername();

            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            
//            boolean access = loginSession.canWrite(grupId, groupName);
            if (!writeAccess) {
                throw new Exception("Anda Tidak Mempunyai Akses Menyimpan Data .");
            } else {
                TUserGroup gr = (TUserGroup) selectedNode.getData();
                administrasiSession.deleteUserGroup(gr);

                selectedNode.getParent().getChildren().remove(selectedNode);
                selectedNode.setParent(null);

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Berhasil",
                        "Penghapusan User Group berhasil"));
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam menghapus User Group "
                    + ex.toString()));
        }
    }

    public boolean isWriteAccess() {
        return writeAccess;
    }

    public void setWriteAccess(boolean writeAccess) {
        this.writeAccess = writeAccess;
    }
}