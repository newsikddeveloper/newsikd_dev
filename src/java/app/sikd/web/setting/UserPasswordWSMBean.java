/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.web.setting;

import app.sikd.entity.mgr.TUserAccount;
import app.sikd.entity.mgr.UserAccount;
import app.sikd.util.Crypto;
import app.sikd.util.SIKDUtil;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Dell
 */

@ManagedBean(name="userPassordWSMBean") 
@RequestScoped 
public class UserPasswordWSMBean {
    private String userName = "";
    private String password = "";
    
    @PostConstruct
    public void init(){
        try {
            UserAccount ua = new UserAccount();   
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext excontext = fc.getExternalContext();
            HttpServletRequest req = (HttpServletRequest)excontext.getRequest();
            TUserAccount uac = (TUserAccount)req.getSession().getAttribute(SIKDUtil.SESSION_USE);
            ua.setUserName(uac.getUsername());
            ua.setPassword(uac.getPassword());
            userName = ua.getUserName().trim();
                        
            byte[] p = Crypto.encrypt(ua.getPassword());
            password ="";
            for (int i = 0; i < p.length; i++) {
                password = password+p[i];
            }
            
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException ex) {
            Logger.getLogger(UserPasswordWSMBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan", "Gagal Inisialisasi " + ex.toString()));
            
        } catch (Exception ex) {
            Logger.getLogger(UserPasswordWSMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
}
