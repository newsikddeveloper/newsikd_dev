package app.sikd.web.setting;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.Pemda;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author donfebi
 */
@ManagedBean(name = "dataPemdaMBean")
@ViewScoped
public class DataPemdaMBean implements Serializable {
    private List<Pemda> allPemdas;
    private Pemda selectedPemda;

    AdministrasiSessionBeanRemote administrasiSession;
    NewLoginSessionBeanRemote loginSession;
    long grupId;
    boolean writeAccess;

    TreeNode root;
    TreeNode selectedNode;
    boolean add;
    boolean editHapus;
    boolean saveCancel;
    int mode=0;
    
    List<SelectItem> jenisPemdas = new ArrayList();

    @PostConstruct
    void init() {
        try {
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext excontext = context.getExternalContext();
            HttpServletRequest httpServletRequest = (HttpServletRequest) excontext.getRequest();
            String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }
            
            jenisPemdas.add(new SelectItem((short)-1, ""));
            jenisPemdas.add(new SelectItem((short)0, "Provinsi"));
            jenisPemdas.add(new SelectItem((short)1, "Kota"));
            jenisPemdas.add(new SelectItem((short)2, "Kabupaten"));

            Context ctxLogin = ServerUtil.getLoginContext();
            administrasiSession = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            writeAccess = loginSession.canWrite(grupId, namamenu);
            root = new DefaultTreeNode("root", null);
            selectedNode=null;
            selectedPemda=new Pemda();

            allPemdas = administrasiSession.getPemdas();
            add= true;
            initData();
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam mengambil data Pemda. "
                    + ex.toString()));
        }
    }
    
    void initData(){
        root = new DefaultTreeNode("root", null);
        if (allPemdas != null) {
                for (int i = 0; i < allPemdas.size(); i++) {
                    Pemda get = allPemdas.get(i);
                    TreeNode tn = new DefaultTreeNode(get, root);
                }
            }
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }
    
    public void onAdd(){
        selectedNode = null;
        selectedPemda = new Pemda();
        initData();
        saveCancel=true;
        editHapus=false;
        add= false;
        mode=1;
    }
    
    public void onEdit(){
        saveCancel=true;
        add=false;
        editHapus=false;
        mode=2;
    }
    
    public void onNodeSelected(NodeSelectEvent event) {        
        TreeNode node = event.getTreeNode();
        selectedPemda = (Pemda) node.getData();
        editHapus = true;
    }

    public void onNodeExpanded(NodeExpandEvent event) {
        System.out.println("onNodeExpanded");
    }

    public void onNodeCollapsed(NodeCollapseEvent event) {
        System.out.println("onNodeCollapsed");
    }

    public boolean isWriteAccess() {
        return writeAccess;
    }

    public void setWriteAccess(boolean writeAccess) {
        this.writeAccess = writeAccess;
    }

    Pemda getNewData () throws Exception{
        if(selectedPemda == null ) selectedPemda = new Pemda();
        if(selectedPemda.getKodeProvinsi()==null || selectedPemda.getKodeProvinsi().trim().equals(""))
            throw new Exception("Silahkan isi Kode Provinsi");
        if(selectedPemda.getKodePemda()==null || selectedPemda.getKodePemda().trim().equals(""))
            throw new Exception("Silahkan isi Kode Pemda");
        if(selectedPemda.getKodeSatker()==null || selectedPemda.getKodeSatker().trim().equals(""))
            throw new Exception("Silahkan isi Kode Satuan Kerja");
        if(selectedPemda.getNamaPemda()==null || selectedPemda.getNamaPemda().trim().equals(""))
            throw new Exception("Silahkan isi Nama Pemda");
        if(selectedPemda.getTipePemda()<0)
            throw new Exception("Silahkan Pilih Jenis Pemda");
        

        return selectedPemda;
    }
    
    public void onSave(){
        try {
            Pemda wk = getNewData();
            if (mode == 1) {
                wk = administrasiSession.createPemda(wk);
                TreeNode tr = new DefaultTreeNode(wk, root);
                allPemdas.add(wk);
            }
            else if (mode == 2){
                administrasiSession.updatePemda(wk);                
            }
            selectedNode = null;
                selectedPemda = new Pemda();
                saveCancel = false;
                add = true;
                editHapus = false;
                initData();
            mode=0;
        } catch (Exception ex) {
            Logger.getLogger(DataPemdaMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onCancel() {
        selectedNode = null;
        selectedPemda = new Pemda();
        saveCancel=false;
        add=true;
        editHapus=false;
        initData();
    }
    public void onDelete(){
        try {
            administrasiSession.deletePemda(selectedPemda);
            saveCancel = false;
            add = true;
            editHapus = false;
            init();
        } catch (Exception ex) {
            Logger.getLogger(DataPemdaMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public long getGrupId() {
        return grupId;
    }

    public void setGrupId(long grupId) {
        this.grupId = grupId;
    }

    public boolean isEditHapus() {
        return editHapus;
    }

    public void setEditHapus(boolean editHapus) {
        this.editHapus = editHapus;
    }

    public Pemda getSelectedPemda() {
        return selectedPemda;
    }

    public void setSelectedPemda(Pemda selectedPemda) {
        this.selectedPemda = selectedPemda;
    }

    public boolean isSaveCancel() {
        return saveCancel;
    }

    public void setSaveCancel(boolean saveCancel) {
        this.saveCancel = saveCancel;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public List<SelectItem> getJenisPemdas() {
        return jenisPemdas;
    }

    public void setJenisPemdas(List<SelectItem> jenisPemdas) {
        this.jenisPemdas = jenisPemdas;
    }
    
}