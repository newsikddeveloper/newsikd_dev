package app.sikd.web.setting;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.backoffice.setting.StatusData;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.office.ejb.newsession.NewSettingSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author soraturaes
 */
@ManagedBean(name = "statusVerifikasiMBean")
@ViewScoped
public class StatusVerifikasiMBean implements Serializable {
    List<StatusData> statusDatas;
    NewSettingSessionBeanRemote settingSession;
    NewLoginSessionBeanRemote loginSession;
    long grupId;
    boolean writeAccess;

    @PostConstruct
    void init() {
        try {
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext excontext = context.getExternalContext();
            HttpServletRequest httpServletRequest = (HttpServletRequest)excontext.getRequest();
            String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
                        
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }

            Context ctxLogin = ServerUtil.getLoginContext();
            settingSession = (NewSettingSessionBeanRemote) ctxLogin.lookup("NewSettingSessionBean/remote");
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            writeAccess = loginSession.canWrite(grupId, namamenu);
            statusDatas = new ArrayList();
            if (loginSession.canRead(grupId, namamenu)) {
                statusDatas = settingSession.getStatusDatas();
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam mengambil data Status Verifikasi. "
                    + ex.toString()));
        }
    }

    public List<StatusData> getStatusDatas() {
        return statusDatas;
    }

}