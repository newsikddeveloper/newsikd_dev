package app.sikd.web.setting;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.backoffice.setting.Account;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.office.ejb.newsession.NewSettingSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author soraturaes
 */
@ManagedBean(name = "cashAccountMBean")
@ViewScoped
public class CashAccountMBean implements Serializable {
    private TreeNode root;

    NewSettingSessionBeanRemote settingSession;
    NewLoginSessionBeanRemote loginSession;
    long grupId;
    boolean writeAccess;

    @PostConstruct
    void init() {
        try {
            System.out.println("init deui");
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext excontext = context.getExternalContext();
            HttpServletRequest httpServletRequest = (HttpServletRequest)excontext.getRequest();
            String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
                        
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }

            Context ctxLogin = ServerUtil.getLoginContext();
            settingSession = (NewSettingSessionBeanRemote) ctxLogin.lookup("NewSettingSessionBean/remote");
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            writeAccess = loginSession.canWrite(grupId, namamenu);
            root = new DefaultTreeNode("root", null);
            
            if (loginSession.canRead(grupId, namamenu)) {
                List<Account> accs = settingSession.getSuperCashAccount();
            
                for (int i = 0; i < accs.size(); i++) {
                    TreeNode aa = new DefaultTreeNode(accs.get(i), root);                    
                }
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam mengambil data Kode Rekening PMDN 13. "
                    + ex.toString()));
        }
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public void onNodeSelected(NodeSelectEvent event) {
        TreeNode node = event.getTreeNode();
        Account obj = (Account) node.getData();
        if( obj.isGroup() && !obj.isReloadSub() ){            
            try {
                List<Account> subs = settingSession.getSubCashAccount(obj.getIndex());
                ((Account) node.getData()).setSubAccount(subs);
                ((Account) node.getData()).setReloadSub(true);
                for (Account su : subs) {
                    TreeNode accnode = new DefaultTreeNode(su, node);
                }
            } catch (Exception ex) {
                System.out.println("Error bro:" + ex);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Kegagalan",
                        "Gagal dalam mengambil data sub group. "
                        + ex.toString()));
            }
        }
    }

    public void onNodeExpanded(NodeExpandEvent event) {
        System.out.println("onNodeExpanded");
    }

    public void onNodeCollapsed(NodeCollapseEvent event) {
        System.out.println("onNodeCollapsed");
    }

}