/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.mgr.TGroupRight;
import app.sikd.entity.mgr.TMenu;
import app.sikd.entity.mgr.TUserAccount;
import app.sikd.entity.mgr.TUserGroup;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author sora
 */
@ManagedBean(name = "menuMBean")
@SessionScoped
public class MenuMBean implements Serializable {

    NewLoginSessionBeanRemote loginSession;
    private TUserGroup userGroup;
    private MenuModel model;

    @PostConstruct
    public void init() {
        model = new DefaultMenuModel();
        try {            
            Context ctxLogin = ServerUtil.getLoginContext();
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
//        FacesContext context = FacesContext.getCurrentInstance();
//        ValueExpression valExp = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), "#{userMBean}", UserMBean.class);
//        UserMBean userbean = (UserMBean) valExp.getValue(context.getELContext());
//        System.out.println("user di menu " + userbean.getUserAccount().getUsername());
//        userGroup = userbean.getUserAccount().getGroup();
//        createMenu(userbean.getUserAccount().getMenus());
        } catch (Exception ex) {
            Logger.getLogger(MenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String menuDoc;

    public void createMenu(List<TMenu> menus) {
        StringBuilder sb = new StringBuilder();
        long parentId = -1;
        String contextPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        boolean first = true;
        int level = 0;
        for (TMenu menu : menus) {
            TMenu parent = menu.getParentMenu();
            if (parent != null) {
                if (parent.getId() == 0) {
                    for (int i = 0; i < level - 1; i++) {
                        sb.append("</ul>\n");
                        sb.append("</li>\n");
                    }
                    if (!first) {
                        sb.append("</ul>\n");
                    }
                    if (menu.isMenuItem()) {
                        sb.append("<li>\n");
                        sb.append("<a href=\"" + contextPath + "/" + menu.getMasterMenu().getAlamat() + "\">\n"
                                + "<span><small>" + menu.getNama() + "</small></span>\n"
                                + "</a>\n");
                        sb.append("</li>\n");
                    } else {
                        if (!first) {
                            sb.append("</li>");
                        }

                        sb.append("<li class=\"treeview\">\n");
                        sb.append("<a href=\"" + contextPath + "/" + menu.getMasterMenu().getAlamat() + "\">\n"
                                + "<span><small>" + menu.getNama() + "</small></span>\n"
                                + "<i class=\"fa fa-angle-left pull-right\"></i>\n"
                                + "</a>\n");
                        first = false;
                    }
                    level = 0;
                    parentId = 0;
                } else {
                    if (parentId < parent.getId()) {
                        level++;
                        sb.append("<ul class=\"treeview-menu\">\n");
                        sb.append("<li>\n");
                        sb.append("<a href=\"" + contextPath + "/" + menu.getMasterMenu().getAlamat() + "\">\n"
                                + "<span><small>" + menu.getNama() + "</small></span>\n");
                        sb.append("</a>\n");
                        parentId = parent.getId();

                    } else if (parentId > parent.getId()) {
                        sb.append("</ul>\n");
                        sb.append("</li>\n");
                        sb.append("<li>\n");
                        sb.append("<a href=\"" + contextPath + "/" + menu.getMasterMenu().getAlamat() + "\">\n"
                                + "<span><small>" + menu.getNama() + "</small></span>\n"
                                + "</a>\n");
                        parentId = parent.getId();
                        level--;

                    } else {
                        sb.append("<li>\n"
                                + "<a href=\"" + contextPath + "/" + menu.getMasterMenu().getAlamat() + "\">\n"
                                + "<span><small>" + menu.getNama() + "</small></span>\n"
                                + "</a>\n"
                                + "</li>\n");
                        parentId = parent.getId();
                    }
                }
            }
        }
        menuDoc = sb.toString();
    }

    public String getMenuDoc() {
        if (menuDoc == null || menuDoc.trim().equals("")) {
            String username = SessionUtil.getCookieUsername();
            if (username != null && !username.trim().equals("")) {
                if (loginSession.getMenuDoc() == null || loginSession.getMenuDoc().trim().equals("")) {
                    try {
                        TUserAccount user = loginSession.getUserbyName(username);
//                        FacesContext facesContext = FacesContext.getCurrentInstance();
//                        ExternalContext excontext = facesContext.getExternalContext();
//                        HttpServletRequest req = (HttpServletRequest) excontext.getRequest();
//                        ValueExpression valExp2 = facesContext.getApplication().getExpressionFactory().createValueExpression(facesContext.getELContext(), "#{menuMBean}", MenuMBean.class);
//                        MenuMBean menubean = (MenuMBean) valExp2.getValue(facesContext.getELContext());
                        setUserGroup(user.getGroup());
                        createMenu(user.getMenus());
                    } catch (Exception ex) {
                        Logger.getLogger(MenuMBean.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        return menuDoc;
    }

    TGroupRight getMenuRight(TMenu mn) {
        TGroupRight result = null;
        boolean find = false;
        if (userGroup != null && userGroup.getRights() != null) {
            int i = 0;
            while (i < userGroup.getRights().size() && find == false) {
                if (userGroup.getRights().get(i).getMenu() != null) {
                    if (mn.getId() == userGroup.getRights().get(i).getMenu().getId()) {
                        result = userGroup.getRights().get(i);
                        find = true;
                    }
                }
                i++;
            }
        }
//        if( user.getGroup() == null) result = true;
        return result;

    }

    int getParent(@SuppressWarnings("UseOfObsoleteCollectionType") java.util.Vector vr, @SuppressWarnings("UseOfObsoleteCollectionType") java.util.Vector vm, long id) {
        int result = -1;
        if (vr.size() > 0) {
            int i = 0;
            boolean find = false;
            while (i < vr.size() && find == false) {
                TMenu obj = (TMenu) vr.elementAt(i);
//                System.out.println(obj.getId() + " = " + id);
                if (obj.getId() == id) {
                    find = true;
                    result = i;
                }
                i++;

            }
        }
        return result;
    }

    public TUserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(TUserGroup userGroup) {
        this.userGroup = userGroup;
    }

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

}
