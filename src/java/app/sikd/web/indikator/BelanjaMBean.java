package app.sikd.web.indikator;

import app.sikd.aas.ServerUtil;
import app.sikd.aas.SessionUtil;
import app.sikd.entity.Pemda;
import app.sikd.entity.graph.GraphGlobal;
import app.sikd.login.ejb.session.AdministrasiSessionBeanRemote;
import app.sikd.login.ejb.session.NewLoginSessionBeanRemote;
import app.sikd.office.ejb.session.GraphicSessionBeanRemote;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

/**
 *
 * @author PC04
 */
@ManagedBean(name = "belanjaMBean")
@ViewScoped

public class BelanjaMBean implements Serializable {
    private BarChartModel barModel;
    ChartSeries chartSeries;
    List<String> years;
    String selectedYear;
    String labelYear;
    List<Pemda> provinces;
    List<SelectItem> selectItemProvinces;
    Pemda selectedProvince;

    AdministrasiSessionBeanRemote administrasiSession;
    NewLoginSessionBeanRemote loginSession;
    GraphicSessionBeanRemote graphSession;
    long grupId;

    @PostConstruct
    public void init() {
        try {
            labelYear = "";
            String username = SessionUtil.getCookieUsername();
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext excontext = context.getExternalContext();
            HttpServletRequest httpServletRequest = (HttpServletRequest) excontext.getRequest();
            String path = httpServletRequest.getRequestURI();
            String namamenu = SIKDUtil.getNamaMenu(path);
            if (username == null) {
                throw new Exception("Identitas pengguna tidak bisa ditemukan atau sudah lewat masa sesi koneksi. "
                        + "Pengguna harus kembali melakukan login.");
            }
            String grupids = SessionUtil.getCookieGroupid();
            if (grupids != null) {
                grupId = Long.valueOf(grupids);
            }

            Context ctxLogin = ServerUtil.getLoginContext();
            Context ctxOffice = ServerUtil.getOfficeContext();
            administrasiSession = (AdministrasiSessionBeanRemote) ctxLogin.lookup("AdministrasiSessionBean/remote");
            loginSession = (NewLoginSessionBeanRemote) ctxLogin.lookup("NewLoginSessionBean/remote");
            graphSession = (GraphicSessionBeanRemote) ctxOffice.lookup("GraphicSessionBean/remote");

            chartSeries = new ChartSeries();
            chartSeries.setLabel("");
            chartSeries.set("", 0);

            barModel = new BarChartModel();
            Axis yAxis = barModel.getAxis(AxisType.Y);
            yAxis.setTickCount(6);
            yAxis.setMax(1);

            barModel.addSeries(chartSeries);

            years = new ArrayList();
            provinces = new ArrayList();
            selectItemProvinces = new ArrayList();

            if (loginSession.canRead(grupId, namamenu)) {
                provinces = administrasiSession.getProvinsis();
                for (int i = 0; i < provinces.size(); i++) {
                    String cc = provinces.get(i).getKodeProvinsi() + "." + provinces.get(i).getKodePemda() + " " + provinces.get(i).getNamaPemda();
                    selectItemProvinces.add(new SelectItem(provinces.get(i), cc));
                }
                years = SIKDUtil.getYears((short) 2010);
            }
        } catch (Exception ex) {
            System.out.println("Error bro:" + ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Kegagalan",
                    "Gagal dalam mengambil data User Group. "
                    + ex.toString()));
        }

    }

    double getNilaiGraph(List<GraphGlobal> ls, String kP) {
        double result = 0;
        if (ls != null && !ls.isEmpty()) {
            boolean find = false;
            int ii = 0;
            while (find == false && ii < ls.size()) {
                GraphGlobal gg = ls.get(ii);
                if (gg.getKodePemda().trim().equals(kP.trim())) {
                    result = gg.getNilai();
                    find = true;
                }
                ii++;
            }
        }
        return result;
    }

    public final void setData(List<GraphGlobal> ls) {
        barModel = new BarChartModel();
        barModel.setTitle("");
        barModel.setShowPointLabels(false);
        barModel.setShowDatatip(true);

        barModel.setExtender("ext");
        barModel.setAnimate(true);

        chartSeries = new ChartSeries();
        double nilai = 0;
        if (selectedProvince != null) {
            try {
                String kP = selectedProvince.getKodePemda().trim();
                double nn = getNilaiGraph(ls, kP);
                chartSeries.set(selectedProvince.getNamaPemda().trim(), nn);
                List<Pemda> kabs = administrasiSession.getPemdaByProvinsi(selectedProvince);
                if (kabs != null) {
                    for (Pemda kab : kabs) {
                        kP = kab.getKodeProvinsi().trim() + "." + kab.getKodePemda().trim();
                        nn = getNilaiGraph(ls, kP);
                        chartSeries.set(kab.getNamaPemda().trim(), nn);
                        if (nilai < nn) {
                            nilai = nn;
                        }
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(BelanjaMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        BigDecimal bd = new BigDecimal(nilai);
        bd = bd.setScale(2, BigDecimal.ROUND_UP);//2 -  decimal places
        nilai = bd.doubleValue();
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(nilai);
        yAxis.setTickCount(6);

        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setTickAngle(-75);
        barModel.addSeries(chartSeries);
    }

    public BarChartModel getBarModel() {
        return barModel;
    }

    public String getSelectedYear() {
        return selectedYear;
    }

    public void setSelectedYear(String selectedYear) {
        this.selectedYear = selectedYear;
    }

    public void onYearChange() {
        labelYear = "";
        if (selectedYear != null && !selectedYear.trim().equals("")) {
            labelYear = "TAHUN ANGGARAN " + selectedYear;
            if (selectedProvince != null) {
                System.out.println("asup");
                try {
                    List<Pemda> kabs = administrasiSession.getPemdaByProvinsi(selectedProvince);
                    List<String> kodes = new ArrayList();
                    for (Pemda kab : kabs) {
                        kodes.add(kab.getKodeProvinsi().trim() + "." + kab.getKodePemda().trim());
                    }
                    List<GraphGlobal> gg = graphSession.getTingkatBelanjaModalGraph(Short.valueOf(selectedYear), (short) 0, (short) 1, kodes);
                    setData(gg);
                } catch (Exception ex) {
                    Logger.getLogger(BelanjaMBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    public List<String> getYears() {
        return years;
    }

    public List<Pemda> getProvinces() {
        return provinces;
    }

    public Pemda getSelectedProvince() {
        return selectedProvince;
    }

    public void setSelectedProvince(Pemda selectedProvince) {
        this.selectedProvince = selectedProvince;
    }

    public List<SelectItem> getSelectItemProvinces() {
        return selectItemProvinces;
    }

    public String getLabelYear() {
        return labelYear;
    }

    public void setLabelYear(String labelYear) {
        this.labelYear = labelYear;
    }

}