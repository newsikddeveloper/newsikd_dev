/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.bean;

import app.sikd.web.laporan.NeracaBackBean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nongky
 */
public abstract class ACOAFilteredBackBean extends ABasicFilteredBackBean implements ICOAFilteredBean {

    protected String coaType;

    @Override
    protected void init() {
        super.init();
    }

    @Override
    public String getCoaType() {
        return coaType;
    }

    @Override
    public void setCoaType(String coaType) {
        this.coaType = coaType;
    }

    protected abstract void onChangeProcess() throws Exception;

    @Override
    public void onFilterChanged() {
        if (pemda != null
                && (year != null && year.length() > 0)
                && (coaType != null && coaType.length() > 0)) {
            try {
                onChangeProcess();
            } catch (Exception ex) {
                Logger.getLogger(NeracaBackBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
