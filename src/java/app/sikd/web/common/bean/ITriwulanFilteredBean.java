/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.bean;

import java.util.List;

/**
 *
 * @author Nongky
 */
public interface ITriwulanFilteredBean {

    public List<String> getTriwulans();

    public void setSelectedTriwulan(String _triwulan);

    /**
     *
     * @return
     */
    public String getSelectedTriwulan();
}
