/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.bean;

import app.sikd.entity.Pemda;
import app.sikd.web.common.ASinglePageBackbean;
import app.sikd.web.common.bean.IPemdaFilteredBean;
import app.sikd.web.common.bean.IYearFilteredBean;
import app.sikd.web.common.filter.PemdaFilter;
import app.sikd.web.common.filter.YearFilter;
import app.sikd.web.common.listener.IFilterChangedListener;
import java.util.List;

/**
 *
 * @author Nongky
 */
public abstract class ABasicFilteredBackBean extends ASinglePageBackbean implements IPemdaFilteredBean, IYearFilteredBean, IFilterChangedListener {

    private PemdaFilter pemdaFilter;
    private YearFilter yearFilter;

    protected Pemda pemda;
    protected String year;
    protected String additionalRender;

    protected void init() {
        pemdaFilter = new PemdaFilter();
        yearFilter = new YearFilter();
    }

    @Override
    public List<Pemda> getPemdas() {
        return pemdaFilter.getPemdas();
    }

    @Override
    public List<String> getYears() {
        return yearFilter.getYears();
    }

    @Override
    public void setSelectedPemda(Pemda _pemda) {
        pemda = _pemda;
    }

    @Override
    public Pemda getSelectedPemda() {
        return pemda;
    }

    @Override
    public void setSelectedYear(String _year) {
        year = _year;
    }

    @Override
    public String getSelectedYear() {
        return year;
    }

    public String getSelectedPreviousYear() {
        if (year != null && year.length() > 0) {
            return String.valueOf(new Short(year) - 1);
        } else {
            return "";
        }
    }

    public String getAdditionalRender() {
        return additionalRender;
    }

    public void setAdditionalRender(String additionalRender) {
        this.additionalRender = additionalRender;
    }

}
