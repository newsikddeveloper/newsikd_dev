/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.bean;

import app.sikd.web.common.filter.ArusKasLabel;
import app.sikd.web.laporan.NeracaBackBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nongky
 */
public abstract class AArusKasBackBean extends ABasicFilteredBackBean implements IArusKasLabelFilteredBean {

    private ArusKasLabel arusKasLabel;

    protected String label;

    @Override
    protected void init() {
        super.init();
        additionalRender = ":data-filter-form:aruskas-label-som";
        arusKasLabel = new ArusKasLabel();
    }
    private List<String> arusKasLabels;

    @Override
    public List<String> getArusKasLabels() {
        return arusKasLabels;
    }

    @Override
    public void setSelectedArusKasLabel(String _label) {
        label = _label;
    }

    @Override
    public String getSelectedArusKasLabel() {
        return label;
    }

    @Override
    public void onFilterChanged() {
        if (label != null && label.length() > 0) {
            onChangeProcess();
        } else {
            if (pemda != null && (year != null && year.length() > 0)) {
                try {
                    arusKasLabels = arusKasLabel.getArusKasLabels(new Short(year), pemda.getKodeSatker());
                } catch (Exception ex) {
                    Logger.getLogger(NeracaBackBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public abstract void onChangeProcess();

}
