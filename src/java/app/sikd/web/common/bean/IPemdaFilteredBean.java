/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.bean;

import app.sikd.entity.Pemda;
import java.util.List;

/**
 *
 * @author Nongky
 */
public interface IPemdaFilteredBean {

    public List<Pemda> getPemdas();

    public void setSelectedPemda(Pemda _pemda);

    /**
     *
     * @return
     */
    public Pemda getSelectedPemda();
}
