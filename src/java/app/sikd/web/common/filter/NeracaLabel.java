/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.filter;

import app.sikd.aas.ServerUtil;
import app.sikd.office.ejb.session.LaporanSessionBeanRemote;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class NeracaLabel {

    private LaporanSessionBeanRemote service;

    public NeracaLabel() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (LaporanSessionBeanRemote) context.lookup("LaporanSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(NeracaLabel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<String> getNeracaLabels(short _tahun, short _semester, String _kodeSatker) throws Exception {
        return service.getNeracaLabels(_tahun, _semester, _kodeSatker);

    }

}
