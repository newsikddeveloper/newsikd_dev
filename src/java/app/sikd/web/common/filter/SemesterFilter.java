/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.filter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nongky
 */
public class SemesterFilter {

    public List<String> getSemesters() {
        List<String> semesters = new ArrayList<>();
        for (int i = 1; i <= 2; i++) {
            semesters.add(String.valueOf(i));
        }
        return semesters;
    }

}
