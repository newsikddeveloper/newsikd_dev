/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.filter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nongky
 */
public class MonthFilter {

    public static final String JANUARI = "Januari";
    public static final String FEBRUARI = "Februari";
    public static final String MARET = "Maret";
    public static final String APRIL = "April";
    public static final String MEI = "Mei";
    public static final String JUNI = "Juni";
    public static final String JULI = "Juli";
    public static final String AGISTUS = "Agustus";
    public static final String SEPTEMBER = "September";
    public static final String OKTOBER = "Oktober";
    public static final String NOVEMBER = "November";
    public static final String DESEMBER = "Desember";

    public List<String> getYears() {
        List<String> months = new ArrayList<>();
        months.add(JANUARI);
        months.add(FEBRUARI);
        months.add(MARET);
        months.add(APRIL);
        months.add(MEI);
        months.add(JUNI);
        months.add(JULI);
        months.add(AGISTUS);
        months.add(SEPTEMBER);
        months.add(OKTOBER);
        months.add(NOVEMBER);
        months.add(DESEMBER);

        return months;
    }

}
