/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.web.common.filter;

import app.sikd.aas.ServerUtil;
import app.sikd.office.ejb.session.LaporanSessionBeanRemote;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author Nongky
 */
public class ArusKasLabel {

    private LaporanSessionBeanRemote service;

    public ArusKasLabel() {
        try {
            Context context = ServerUtil.getOfficeContext();
            service = (LaporanSessionBeanRemote) context.lookup("LaporanSessionBean/remote");
        } catch (Exception ex) {
            Logger.getLogger(ArusKasLabel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<String> getArusKasLabels(short _tahun, String _kodeSatker) throws Exception {
        return service.getArusKasLabels(_tahun, _kodeSatker);
    }

}
