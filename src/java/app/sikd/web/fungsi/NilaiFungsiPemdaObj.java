package app.sikd.web.fungsi;

import app.sikd.entity.backoffice.NilaiFungsiPemda;
import app.sikd.util.SIKDUtil;

/**
 *
 * @author sora
 */
public class NilaiFungsiPemdaObj extends NilaiFungsiPemda{

    public NilaiFungsiPemdaObj() {
    }

    public NilaiFungsiPemdaObj(String kodePemda, String namaPemda, double nilai) {
        super(kodePemda, namaPemda, nilai);
    }
    public NilaiFungsiPemdaObj(String noUrut, String kodePemda, String namaPemda, double nilai) {
        super(kodePemda, namaPemda, nilai);
        setKodeFungsi(noUrut);
    }
    
    public void setNoUrut(String noUrut){
        
    }
    public String getNoUrut(){
        return getKodeFungsi();
    }

    public void setNilaiAsString(String nilaiAsString){
        
    }
    public String getNilaiAsString(){
        return SIKDUtil.doubleToString(getNilai());
    }
    
    
}
